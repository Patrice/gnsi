"""
Le module config
================

Permet de lire le fichier de configuration de l'application.
------------------------------------------------------------
"""
import shutil
import logging
import appdirs
from configparser import ConfigParser
from pathlib import Path, PurePath
from .exceptions import *

def get_config():
    """ Renvoie l'objet ConfigParser qui gère le fichier de configuration."""
    config = ConfigParser()
    config_dir = Path(appdirs.user_config_dir("gnsi"))
    config_file = Path(appdirs.user_config_dir("gnsi"))/"config_gnsi.cfg"
    if not config_file.exists():
        raise FileExistsError(f"Le fichier{config_file} n'existe pas.")
    config.read(config_file)
    logging.info("lecture de %s.", config_file)
    return config

def save_new_config_file(confdict):
    """Enregistre le fichier de configuration à partir d'un dictionnaire."""
    config = ConfigParser()
    config.read_dict(confdict)
    config_dir = Path(appdirs.user_config_dir("gnsi"))
    config_file = Path(appdirs.user_config_dir("gnsi"))/"config_gnsi.cfg"
    if not config_file.exists():
        if not config_file.parent.is_dir():
            config_dir.mkdir()
            logging.info("Création de %s.", config_dir)
    with open(config_file, 'w') as configfile:
                config.write(configfile)

def get_dossier(nom):
    config = get_config()
    return config["Paths"][nom]

def get_executable(nom):
    config = get_config()
    return config["Exe"][nom]

def get_dossier_pdf():
    """Renvoie le chemin du dossier pdf."""
    config = get_config()
    return config["Paths"]["pdf"]

def get_fichier_eleves(nom):
    config = get_config()
    return config["Classes"][nom]
    
                
def get_dossier_brouillons():
    """Renvoie le chemin du dossier brouillons."""
    config = get_config()
    return config["Paths"]["brouillons"]

def get_dossier_exercices():
    """Renvoie le chemin du dossier latex."""
    config = get_config()
    return config["Paths"]["exercices"]


def get_dossier_devoirs():
    """Renvoie le chemin du dossier templates."""
    config = get_config()
    return config["Paths"]["devoirs"]
    
def get_brouillon_name():
    """Récupère le nom du fichier présent dans le dossier brouillon."""
    dossier_brouillons = Path(get_dossier_brouillons())
    files = Path(get_dossier_brouillons()).glob('*.md')
    for f in files:
        if "ans3" in f.name or "visualisation" in f.name:
            f.unlink()
    for f in dossier_brouillons.iterdir():
        if "~" not in str(f.name):
            return str(f.name)
    raise GNSIBrouillonEmptyError

def ajouter_classe(nom_classe, nom_fichier):
    """Ajouter la référence à la classe dans le fichier de configuration 
    et copie le fichier dans les modèles."""
    fichier = Path(nom_fichier)
    if fichier.exists():
        nom = PurePath(fichier).name
        dst = Path(get_dossier("modeles")) / nom
        shutil.copyfile(str(fichier), str(dst))
        config = get_config()
        if "Classes" not in config:
            config["Classes"] = {}
        config["Classes"][nom_classe] = str(dst)
        config_dir = Path(appdirs.user_config_dir("gnsi"))
        config_file = Path(appdirs.user_config_dir("gnsi"))/"config_gnsi.cfg"
        with open(config_file, 'w') as configfile:
                    config.write(configfile)


def classe_existe(nom_classe):
    """Précise si la classe est enregistrée."""
    config = get_config()
    return "Classes" in config and nom_classe in config["Classes"]

def get_nom_exercice():
    """Renvoie le nom par defaut des exercices."""
    config = get_config()
    return config["Defaut"]["nom"]
