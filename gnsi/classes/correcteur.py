
"""
Le module qui corrige les devoirs.
"""

import shutil
import subprocess
import os
import logging
from collections import OrderedDict
from lxml import etree
from pathlib import Path, PurePath
from .devoir import *
from ..config import get_dossier, get_executable, classe_existe, get_fichier_eleves
from ..exceptions import GNSINameDirectoryError

class Correcteur:
    def __init__(self, devoir):
        self.nom_devoir = devoir
        self.dossier_devoir = Path(get_dossier("devoirs")) /devoir
        if self.dossier_devoir.exists():
            self.devoir = DevoirCorrection(devoir)
        else:
            raise (GNSINameDirectoryError)
        self.bareme_par_exercice = self.get_bareme_par_exercice()
        self.bareme = self.get_bareme()
        self.eleves = self.get_donnees_eleves()
        self.total_points = self.get_total_points(self.bareme)

    def get_bareme_par_exercice(self):
        """
        Lit le fichier bareme.csv du devoir classé par exercices et le
        renvoie sous forme de dictionnaire.
        """
        bareme = OrderedDict()
        fichier_bareme = self.dossier_devoir / "bareme.csv"
        with open(fichier_bareme, "r") as f:
            reader = csv.reader(f)
            for row in reader:
                cle = row[1].replace('.','_')
                if row[0] not in bareme:
                    bareme[row[0]] = {}
                bareme[row[0]][cle] = int(row[2])
        return bareme

    def get_bareme(self):
        """
        Lit le fichier bareme.csv du devoir et le renvoie sous forme
        de dictionnaire.
        """
        bareme = {}
        fichier_bareme = self.dossier_devoir / "bareme.csv"
        with open(fichier_bareme, "r") as f:
            reader = csv.reader(f)
            for row in reader:
                cle = row[1].replace('.','_')
                bareme[cle] = int(row[2])
        return bareme

    def get_donnees_eleves(self):
        """
        Renvoie sous forme de dictionnaire les données élèves (idmoodle, email, ...)
        lues à partir du fichier classe déclarée dans la configuration.
        """
        fichier_eleves = Path(get_fichier_eleves(self.devoir.classe))
        print(fichier_eleves)
        eleves = OrderedDict()
        with open(fichier_eleves, "r") as f:
            reader = csv.DictReader(f)
            for row in reader:
                eleves[row['eleve']] = {}
                eleves[row['eleve']]['scores'] = {}
                eleves[row['eleve']]['note_brute'] = 0
                eleves[row['eleve']]['note'] = 0
                eleves[row['eleve']]['erreurs'] =  ""
                # nom_fichier = str(self.devoir.dossier_bilans) +"/"+ row['eleve'].replace(" ","_")+".md"
                nom_fichier = row['eleve'].replace(" ","_")+".md"
                eleves[row['eleve']]['fichier'] =  nom_fichier
                for valeur in ['id', 'idmoodle', 'prenom', 'nom', 'email']:
                    eleves[row['eleve']][valeur] = row[valeur]
        return eleves

    def get_total_points(self, dict_score):
        """ Calcul le nombre total de points du bareme """
        total = 0
        for cle, valeur in dict_score.items():
            total += valeur
        return total
    
    def corriger(self):
        """
        Corriger les devoirs élèves.
        """
        os.chdir(str(self.dossier_devoir))
        testeur = get_executable("test")
        # Pour chaque élève, on importe ses travaux puis lance les tests.
        for eleve in self.devoir.dossier_travaux.iterdir():
            nom_eleve = PurePath(eleve).name
            print("Correction de ", nom_eleve)
            #Supprimer tous les fichiers .py de la racine du dossier devoir.
            for f in self.dossier_devoir.glob("*.py"):
                f.unlink()
            # on recopie les fichier eleves    
            for fichier in eleve.iterdir():
                nom = PurePath(fichier).name
                dst = self.dossier_devoir / nom
                shutil.copyfile(str(fichier), str(dst))
            # on lance les tests    
            subprocess.call([testeur])
            tree = etree.parse("nosetests.xml")
            total = 0
            for resultat in tree.xpath("/testsuite/testcase"):
                test = resultat.get("name")
                if list(resultat) == []:
                    score = self.bareme[test]
                else:
                    score = 0
                    for m in list(resultat):
                        self.eleves[nom_eleve]['erreurs'] +=  m.get("message")
                                            
                total += score
                self.eleves[nom_eleve]['scores'][test] = score
            self.eleves[nom_eleve]['note_brute'] = self.get_total_points(self.eleves[nom_eleve]['scores'])
            self.eleves[nom_eleve]['note'] = round(total * 20/self.total_points, 2)


    def exporter_resultats_markdown(self):
        """
        Exporte les scores élèves dans un fichier markdown.
        """
        for eleve in self.eleves.keys():
            nom_fichier_bilan = eleve.replace(' ','_') +".md"
            fichier_bilan = Path(self.devoir.dossier_bilans) / nom_fichier_bilan

            text = f"---\ntitle : {self.devoir.titre}\nauthor : {eleve}\ndate : {self.devoir.auteur}\n---\n\n"
            text += f"# Note : {self.eleves[eleve]['note']} / 20\n\n"
            for exercice in self.bareme_par_exercice.keys():
                text += f"## {exercice}\n"
                for question, points in self.bareme_par_exercice[exercice].items():
                    liste_q = question.split('_')
                    num_question = ".".join(liste_q[2:-1])
                    if question in self.eleves[eleve]['scores']:
                        score_question = self.eleves[eleve]['scores'][question]
                    else:
                        score_question = 0
                    text += f"- **{num_question}** : {score_question} / {points}\n"
                text += "\n"

            text += "# Détails erreurs\n\n"
            text += "```{python}\n"
            text += self.eleves[eleve]['erreurs']
            text += "\n```\n"

            fichier_bilan.write_text(text)

    def exporter_fichier_bilan(self):
        """
        Exporter les résultats de la classe dans un fichier csv
        """
        headers_base = ["id", "eleve", "prenom", "nom", "idmoodle", "email","fichier", "note", "note_brute"]
        headers_exos = []
        for exo in self.bareme_par_exercice.keys():
            for nom_test in self.bareme_par_exercice[exo].keys():
                headers_exos.append(nom_test)
        headers = headers_base + headers_exos
        nom_fichier_bilan = f"bilan_{self.nom_devoir}.csv"
        fichier_bilan = Path(self.devoir.dossier_bilans) / nom_fichier_bilan
        with open(fichier_bilan, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=headers)
            writer.writeheader()
            for eleve in self.eleves.keys():
                dico = {"eleve" : eleve}
                for cle in headers_base:
                    if cle not in dico:
                        dico[cle] = self.eleves[eleve][cle]
                for cle in headers_exos:
                    if cle in self.eleves[eleve]['scores'].keys():
                        dico[cle] = self.eleves[eleve]['scores'][cle]
                writer.writerow(dico)
