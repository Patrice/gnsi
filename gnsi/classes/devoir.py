""" Le module qui permet de gérer les devoirs. 
"""

import shutil
import subprocess
from zipfile import ZipFile 
from os import chdir
from pathlib import Path, PurePath
from ..config import get_dossier, get_executable, classe_existe, get_fichier_eleves
from ..exceptions import GNSINameDirectoryError
from .document_base import documentBase
from .exercice import *

class DevoirCorrection:
    def __init__(self, dossier):
        "La classe qui gère un exercice."
        self.dossier_devoir = Path(get_dossier("devoirs")) /dossier
        if self.dossier_devoir.exists():
            donnees = self.dossier_devoir / "devoir.txt"
            text = donnees.read_text()
            titre, classe, auteur, date, exos, *residus = text.split("\n")
            self.titre = titre
            self.auteur = auteur
            self.date = date
            self.exercices = exos.split(",")
            self.classe = classe
        else:
            self.dossier_devoir.mkdir()
            sous_dossiers = ["latex", "bilans", "docs", "fichiers_eleves", "travaux"]
            for sd in sous_dossiers:
                rep = self.dossier_devoir / sd
                rep.mkdir()
                
        self.fichier_bareme = self.dossier_devoir/"bareme.csv"
        self.fichier_bareme_amc = self.dossier_devoir/"bareme_amc.csv"
        self.dossier_latex = self.dossier_devoir/"latex"
        self.fichier_latex = self.dossier_latex / f"{dossier}.tex"
        self.dossier_docs = self.dossier_devoir/"docs"
        self.dossier_fichiers_eleves = self.dossier_devoir/"fichiers_eleves"
        self.dossier_travaux = self.dossier_devoir/"travaux"
        self.dossier_bilans = self.dossier_devoir/"bilans"
        
    def creer_fichier_latex(self):
        """
        Crée le fichier source et le place dans le dossier latex.
        """
        doc = documentBase(self.titre, self.auteur, self.date)
        for i, exo in enumerate(self.exercices):
            doc.ajouter_exercice(exo, i+1)
        doc.set_destination_file(self.fichier_latex)
        doc.set_destination_dir(self.dossier_latex)
        doc.save()
        doc.save_amc()
        
    def visualiser_pdf(self):
        """
        Lance la compilation et la visualisation du sujet.
        """
        chdir(self.dossier_latex)
        latex = get_executable("latex")
        reader = get_executable("reader")
        subprocess.call([latex, self.fichier_latex])
        pdf = str(self.fichier_latex)[:-3] + "pdf"
        subprocess.call([reader, pdf])

    def importer_fichiers_annexes(self):
        """ Importer tous les fichiers (tests, fichiers élèves, etc...)
        dans l'arborescence du devoir."""
        # on efface le bareme existant
        self.fichier_bareme.write_text("")
        # les tests
        for i, nom in enumerate(self.exercices):
            exercice = Exercice(nom, i+1)
            exercice.envoyer_tests(self.dossier_docs)
            exercice.envoyer_bareme(self.dossier_devoir)
            exercice.envoyer_fichiers_eleves(self.dossier_fichiers_eleves)
        # le fichier setup.cfg pour la sortie xml
        fichier_setup_initial =  Path(get_dossier("modeles")) / "setup_devoir.cfg"
        fichier_setup_final = self.dossier_devoir / "setup.cfg"
        shutil.copyfile(str(fichier_setup_initial), str(fichier_setup_final))

    def ramasser(self, nom_archive, classe):
        """ Importer l'archive zip dans le dossier travaux et la liste des élèves à la racine du dossier devoir."""
        # On vérifie que la classe existe
        if classe_existe(classe):
            fichier_eleves = Path(get_fichier_eleves(classe))
            self.classe = classe
            self.save_config_file()
        else:
            raise GNSIClasseNotExist(classe)
        # d'abord on vide le dossier
        shutil.rmtree(self.dossier_travaux)
        self.dossier_travaux.mkdir()
        # on dezippe
        archive = Path(nom_archive)
        with ZipFile(archive, 'r') as zip:
            zip.extractall(path=self.dossier_travaux) 
        for dir in self.dossier_travaux.iterdir():
            nom_initial = PurePath(dir).name
            l = nom_initial.split("_")
            nom_final = self.dossier_travaux / l[0]
            dir.rename(nom_final)

    def save_config_file(self):
        donnees = self.dossier_devoir / "devoir.txt"
        text = self.titre + "\n"
        text += self.classe + "\n"
        text += self.auteur + "\n"
        text += self.date + "\n"
        text += ",".join(self.exercices)
        donnees.write_text(text)

    
class Devoir(DevoirCorrection):
    def __init__(self, dossier, titre, auteur, date, exercices):
        DevoirCorrection.__init__(self, dossier)
        self.titre = titre
        self.auteur = auteur
        self.date = date
        self.exercices = exercices
        self.classe = "None"
        self.save_config_file()
