"""Le document de base pour créer une feuille d'exercices."""

import logging
from .jinja_base import jinjaBase
from .exercice import Exercice
from ..exceptions import *

class documentBase(jinjaBase):
    """documentBase gère le comportement par défaut d'un fichier à générer.

    Attributs
    ---------
    titre (str): le titre du document, sera également utilisé pour générer les fichiers tex et pdf.
    auteur (str): l'auteur du document.
    date(str) : la date de génération du document.
    template_entete (défaut = "document_debut.tex"): le fichier template d'entête du document tex.
    template_fin (défaut="document_fin.tex") : le template contenant la fin du document tex.
    exercices (defaut = []): la liste des fichiers à inclure dans le document.
    destination_file (defaut = None) : le nom du fichier généré, sera à préciser avant de lancer la création du document.
    destination_dir (defaut = None) : le nom du dossier où sera enregistré le document, sera à préciser avant de lancer la création du document.
    """
    def __init__(self, titre="Aucun", auteur="Aucun", date="Aucun"):
        super().__init__()
        self.titre = titre
        self.auteur = auteur
        self.date = date
        self.template_entete = "document_debut.tex"
        self.template_fin = "document_fin.tex"
        self.template_amc_entete = "document_correction_debut.tex"
        self.template_amc_question = "document_amc_question{}.tex"
        self.template_amc_fin = "document_correction_fin.tex"
        self.exercices = []
        self.destination_file = None
        self.destination_dir = None
        logging.info("Création du document %s.", titre)

    def ajouter_exercice(self, nom, n):
        """Ajoute un exercice à la liste des exercices."""
        
        self.exercices.append(Exercice(nom, n))
        
    def valeurs_entete(self):
        """Renvoie le titre, l'auteur et la date sous forme de dictionnaire."""
        valeurs = {"titre": self.titre,
                   "auteur" : self.auteur,
                   "date": self.date
        }
        return valeurs

    def set_auteur(self, nom):
        self.auteur = nom
    
    def get_destination_file(self):
        return self.destination_file

    def get_destination_dir(self):
        return self.destination_dir

    def set_destination_file(self,nom):
        self.destination_file = nom

    def set_destination_dir(self,nom):
        self.destination_dir = nom

    def generate_latex(self):
        """Génère le code latex du document.""" 
        contenu = self.get_latex(self.template_entete, self.valeurs_entete())
        for exo in self.exercices:
            # on récupère le latex et on l'ajoute
            enonce = exo.get_latex()
            contenu += enonce
            # on envoye les images également
            exo.envoyer_images(self.destination_dir)
        contenu += self.get_latex(self.template_fin,{})
        return contenu

    def generate_latex_amc(self):
        """Génère le fichier latex au format AMC pour la correction."""
        
        contenu = self.get_latex(self.template_amc_entete, self.valeurs_entete())

        for exercice in self.exercices:
            contenu += "\section{Exercice "+ str(exercice.numero) + "}\n\n"
            for valeurs, point in exercice.get_valeurs_amc():
                template = self.template_amc_question.format(point)
                contenu += self.get_latex(template,valeurs)
        
        contenu += self.get_latex(self.template_amc_fin,{})
        return contenu
        

    def save(self):
        if self.destination_dir is None or self.destination_file is None:
            raise GNSIDocumentError
        with open(self.destination_file, "w") as f:
            f.write(self.generate_latex())

    def save_amc(self):
        if self.destination_dir is None or self.destination_file is None:
            raise GNSIDocumentError
        fichier_amc = str(self.destination_file)[:-4] + "_amc.tex"
        with open(fichier_amc, "w") as f:
            f.write(self.generate_latex_amc())
