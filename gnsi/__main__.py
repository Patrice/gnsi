"""Le programme de gestion des évaluations de la NSI en ligne de commandes."""
import shutil
from pathlib import Path, PurePath
import begin
import gnsi.config as config
import gnsi.exceptions as exceptions
import gnsi.cli as cli
import gnsi.utils as utils


@begin.subcommand
def configuration():
    """Affiche le fichier de configuration."""
    try :
        conf = config.get_config()
        for section in conf.sections():
            for nom, valeur in conf.items(section):
                print(nom, " : ", valeur)
    except exceptions.GNSIConfigError as erreur:
        utils.message_erreur(erreur.message)

@begin.subcommand
def classe(ajouter: "Ajoute une classe à la base de données" = False,
           defaut: "déclare la classe par défaut." = False,
           existe:"Précise si la classe existe dans le fichier de config." = False,
           *args:"nom de la classe [, un fichier csv avec les données élèves.]"):
    """Permet de gérer les fichiers avec les données élèves."""
    if ajouter or defaut:
        try:
            if len(args) < 2:
                raise(exceptions.GNSIArgumentError)
            if ajouter:
                config.ajouter_classe(args[0], args[1])
            if defaut:
                config.ajouter_classe("defaut", args[1])
        except exceptions.GNSIConfigError as erreur:
            utils.message_erreur(erreur.message)
        except exceptions.GNSIArgumentError as erreur:
            utils.message_erreur(erreur.message)
    elif existe:
        try:
            if len(args) < 1:
                raise(exceptions.GNSIArgumentError)
            print(config.classe_existe(args[0]))
        except exceptions.GNSIConfigError as erreur:
            utils.message_erreur(erreur.message)
        except exceptions.GNSIArgumentError as erreur:
            utils.message_erreur(erreur.message)
    else:
        utils.message_erreur("Option inconnue !")


@begin.subcommand
def new(dossier: "Dossier à créer dans votre home" = "GNSIcontenus"):
    """Créer les dossiers et le fichier de configuration adéquate."""
    dossier = "GNSIcontenus"
    reponse = input(f"Nom du dossier (par défaut : {dossier}) :")
    if reponse != "":
        dossier = reponse
    sous_dossiers = ["brouillons", "brouillons/docs", "brouillons/latex", "exercices", "devoirs", "modeles"]
    userhome = Path().home()/dossier
    dossiers = [userhome] + [userhome/i for i in sous_dossiers]
    for rep in dossiers:
        if not rep.exists():
            rep.mkdir()
    confdict = {"Paths":{}}
    dossiers = dossiers[1:]
    for i, ssdossier in enumerate(dossiers):
        confdict["Paths"][sous_dossiers[i]] = str(ssdossier)
    ## On rajoute les executables par defaut:
    confdict["Exe"]={}
    confdict["Exe"]["latex"] = "xelatex"
    confdict["Exe"]["reader"] = "evince"
    confdict["Exe"]["test"] = "nosetests3"
    ## les valeurs par defaut
    confdict["Defaut"]={}
    nomexo = "exercice"
    reponse = input(f"Nom de l'exercice, à personnaliser si vous voulez partager sur framagit (par défaut : {nomexo}) :")
    if reponse != "":
        nomexo = reponse
    confdict["Defaut"]["nom"] = nomexo
    # On enregistre le tout   
    config.save_new_config_file(confdict)
    ## remplit le dossier avec les modeles par defaut
    dossier_modeles = userhome / "modeles"
    dossier_data = Path(__file__).parent / "data"
    for f in dossier_data.iterdir():
        if not f.is_dir():
            dst = dossier_modeles / PurePath(f).name
            shutil.copyfile(str(f), str(dst))
    

@begin.subcommand
def brouillon(clean: "Vide le dossier brouillons" = False,
              importer: "Importer les fichier (adresse absolue) dans le dossier brouillons." = False,
              tester: "Génère tous les formats à partir du fichier markdown et lance les tests. " = False,
              force:"Impose la regénération du fichier latex" = False,
              pdf:"Lance la compilation du fichier latex et visualise le pdf." = False,
              preparer: "Nomme l'exercice et génère le pdf avec ce nom."
              = False,
              exporter: "Ajoute l'exercice à la base de données et l'enlève des brouillons."
              = False,
              corriger: "Réimporte un exercice existant pour le corriger." = False,
              *args):
    """Permet de manipuler le brouillon de l'exercice avant de l'ajouter à la base."""

    if clean:
        cli.clean_brouillons()
    elif importer:
        try:
            cli.import_to_brouillons(args)
        except exceptions.GNSIBrouillonImportFormatError as erreur:
            utils.message_erreur(erreur.message)
        except exceptions.GNSIBrouillonNotEmptyError as erreur:
            utils.message_erreur(erreur.message)
        except exceptions.GNSIFileNotFoundError as erreur:
            utils.message_erreur(erreur.message)
    elif tester:
        try:
            cli.tester_brouillon(force)
        except exceptions.GNSITemplateError as erreur:
            utils.message_erreur(erreur.message)

    elif pdf:
        cli.visualiser_pdf()
    elif preparer:
        try:
            cli.preparer()
        except exceptions.GNSIBrouillonEmptyError as erreur:
            utils.message_erreur(erreur.message)
    elif exporter:
        try:
            cli.exporter()
        except exceptions.GNSITemplateError as erreur:
            utils.message_erreur(erreur.message)
    elif corriger:
        try:
            cli.corriger_exercice(args[0])
        except exceptions.GNSINameDirectoryError as erreur:
            utils.message_erreur(erreur.message)
        except exceptions.GNSIBrouillonNotEmptyError as erreur:
            utils.message_erreur(erreur.message)
    else:
        utils.message_erreur("Option inconnue !")


@begin.subcommand
def devoir(corriger: "Pour mettre à jour un devoir existant" = False,
          *args : "nom du dossier à créer, le nom du devoir sur le sujet imprimé, l'auteur du devoir ou le niveau où il est donné, la date ou l'année scolaire et les exercices à inclure dans le devoir"):
    """ Crée un dossier devoir,sa feuille de correction AMC,
    ainsi que la batterie de tests qui permettent une autoévaluation."""
    if corriger:        
        try:
            cli.corriger_devoir(args[0])
        except exceptions.GNSIArgumentError as erreur:
            utils.message_erreur(erreur.message)
    else:
        try:
            dossier, nom, auteur, date, *exercices = args
            cli.creer_devoir(dossier, nom, auteur, date, exercices)
        except exceptions.GNSIArgumentError as erreur:
            utils.message_erreur(erreur.message)

@begin.subcommand
def ramasser(*args : "- le nom du devoir (nom du dossier),\n - l'archive générée par Moodle,\n - la classe concernée (classe par défaut si rien.)"):
    """ Place l'archive Moodle dans le dossier du devoir."""
    try:
        print(args)
        if len(args) == 3:
            nom_dossier, nom_archive, classe = args
        elif len(args) == 2:
            nom_dossier, nom_archive = args
            classe = "defaut"
        else:
            raise(ValueError)
        cli.ramasser(nom_dossier, nom_archive, classe)
    except exceptions.GNSIArgumentError as erreur:
        utils.message_erreur(erreur.message)
    except exceptions.GNSINotZipFileError as erreur:
        utils.message_erreur(erreur.message)
    except ValueError as erreur:
        utils.message_erreur("Le nombre d'arguments est incorrect.")

@begin.subcommand
def corriger(devoir: "le nom du devoir (nom du dossier)."):
    """Corrige les travaux élèves qui ont été ramassés et placés dans le dossier du devoir."""

    try:
        cli.corriger(devoir)
    except exceptions.GNSINameDirectoryError as erreur:
        utils.message_erreur(erreur.message)

        
@begin.start
def cli_start():
    """Fonction principale"""
