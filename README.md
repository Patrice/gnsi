# GNSI

*Ce programme est un assistant pour le prof de NSI. C'est donc assez ciblé mais il m'est d'avis qu'il peut être utile à certains.*

L'idée est de pouvoir créer des devoirs autocorrigés à faire sur
machine puis déposés sur Moodle. Mais, les épreuves étant écrites, on
doit pouvoir le faire sous cette forme. Même si cela peut
paraître inutile, il est possible de corriger ces copies en utilisant
une feuille AMC ([Auto Multiple Choice](https://www.auto-multiple-choice.net/)).

C'est pourquoi gnsi sert à :

* **Objectif 1** : imposer un cadre rigide pour aider à la conception d'une base de données d'exercices écrits au format markdown.
* **Objectif 2** : créer à partir des exercices enregistrés dans la base des devoirs sous 2 versions :
   - écrite : un sujet au format tex puis pdf ainsi qu'une feuille de correction au format latex pour l'utiliser dans le logiciel AMC ;
   - numérique : les énoncés en version markdown qui peuvent servir de doctest ainsi que les fichiers .py à fournir aux élèves.
* **Objectif 3** : importer le dossier zip des travaux élèves déposés sur Moodle dans le dossier du devoir.
* **Objectif 4** : corriger les travaux numériques :
   - pour les élèves : générer un fichier markdown avec la note, les détails des tests ratés et les points ;
   - pour le prof : un fichier csv avec les scores élèves prêt à être déposé dans le carnet de notes Moodle.

Pour les curieux qui tenteront l'aventure, il y en plus la possibilité
de détecter les éléves qui ont copiés grâce au logiciel
[copydetect](https://pypi.org/project/copydetect/).

Voir la [documentation complète sur framgit](https://patrice.frama.io/gnsi/) ou [documentation complète dans la forge](https://patrice_le_borgne.forge.aeif.fr/gnsi/).

